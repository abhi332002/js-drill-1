// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*"

//Define function 
function getLastCarInfo(inventory) {

    // It will check inventory not an array or not empty
    if (!Array.isArray(inventory) || inventory.length === 0) {
        console.log("Invalid inventory. Please provide a non-empty array.");
        return []; // Return an empty array if inventory is invalid or empty
    }

    let carData = null;
    let carCounts = inventory.length;

    if (carCounts > 0) {
        carData = inventory[carCounts - 1]
    }
    return carData;
}

//export the function 
module.exports = { getLastCarInfo };