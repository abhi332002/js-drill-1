const { inventory } = require("../carDataSet");
const { getLastCarInfo } = require("../problem2");

let carData = getLastCarInfo(inventory);

if (carData == null) {
    console.log("Car data not found");
} else {
    console.log(`Last car is a ${carData.car_make} ${carData.car_model} `);
}