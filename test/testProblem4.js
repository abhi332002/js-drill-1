const { inventory } = require("../carDataSet");
const { getAllCarYearInfo } = require("../problem4");


let carYearData = getAllCarYearInfo(inventory);

if (carYearData) {
    console.log(carYearData);
} else {
    console.log('Car data not found');
}