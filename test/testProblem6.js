const { inventory } = require("../carDataSet");
const { filterBmwAndAudiCars } = require("../problem6");


let bmwAndAudiCar = filterBmwAndAudiCars(inventory);

if (bmwAndAudiCar) {
    console.log(JSON.stringify(bmwAndAudiCar));
} else {
    console.log("Car data not found");
}