//In this file we test our problem-1

const { inventory } = require("../carDataSet");
const { getCarInfo } = require("../problem1");


let carData = getCarInfo(inventory,33);

if(carData==null){
    console.log("Car data not found");
}else{
    console.log(`Car ${carData.id} is ${carData.car_year} ${carData.car_make} ${carData.car_model} `);
}