const { inventory } = require("../carDataSet");
const { sortCarModels } = require("../problem3");


let carModels = sortCarModels(inventory);

if (carModels) {
    console.log(carModels);
} else {
    console.log("car data not found");
}