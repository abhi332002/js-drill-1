const { inventory } = require("../carDataSet");
const { getOldCars } = require("../problem5");


let oldCarsArr = getOldCars(inventory);

if (oldCarsArr) {
    console.log(oldCarsArr);
} else {
    console.log("car data not found");
}