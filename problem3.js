// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

//Define function 
function sortCarModels(inventory) {

    // It will check inventory not an array or not empty
    if (!Array.isArray(inventory) || inventory.length === 0) {
        console.log("Invalid inventory. Please provide a non-empty array.");
        return []; // Return an empty array if inventory is invalid or empty
    }


    let carModelArr = [];

    //copy the model data in car model array
    for (let carIndex = 0; carIndex < inventory.length; carIndex++) {
        carModelArr.push(inventory[carIndex].car_model);
    }

    //sort the car models array
    carModelArr.sort();

    //return the array
    return carModelArr;

}

// export the function
module.exports = { sortCarModels };