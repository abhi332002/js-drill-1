// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

//Define function 
function getCarInfo(inventory, carId) {

    // It will check inventory not an array or not empty
    if (!Array.isArray(inventory) || inventory.length === 0) {
        console.log("Invalid inventory. Please provide a non-empty array.");
        return []; // Return an empty array if inventory is invalid or empty
    }

    let carData = null;
    for (let carIndex = 0; carIndex < inventory.length; carIndex++) {

        if ((carIndex + 1) == carId) {
            carData = inventory[carIndex];
            return carData;
        }
    }
}

//export the function
module.exports = { getCarInfo };