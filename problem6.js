// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

//Define Function 
function filterBmwAndAudiCars(inventory) {

    // It will check inventory not an array or not empty
    if (!Array.isArray(inventory) || inventory.length === 0) {
        console.log("Invalid inventory. Please provide a non-empty array.");
        return []; // Return an empty array if inventory is invalid or empty
    }

    let bmwAndAudiCar = [];

    for (let carIndex = 0; carIndex < inventory.length; carIndex++) {
        if (inventory[carIndex].car_make == 'BMW' || inventory[carIndex].car_make == 'Audi') {
            bmwAndAudiCar.push(inventory[carIndex]);
        }
    }
    return bmwAndAudiCar;
}

//export function 
module.exports = { filterBmwAndAudiCars };