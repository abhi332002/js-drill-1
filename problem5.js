// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const { getAllCarYearInfo } = require("./problem4");

//Define function 
function getOldCars(inventory) {

    // It will check inventory not an array or not empty
    if (!Array.isArray(inventory) || inventory.length === 0) {
        console.log("Invalid inventory. Please provide a non-empty array.");
        return []; // Return an empty array if inventory is invalid or empty
    }

    
    let allCars = [];
    let oldCarsArr = [];
    let oldCars = 0;

    //first take all cars year from previous problems 
    allCars = getAllCarYearInfo(inventory);

    //take old cars 
    for (let carIndex = 0; carIndex < allCars.length; carIndex++) {
        if (allCars[carIndex] < 2000) {
            oldCarsArr.push(allCars[carIndex]);
            oldCars++;
        }
    }
    console.log("Total old cars is ", oldCars)
    return oldCarsArr;
}

// export function
module.exports = { getOldCars };