// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

//Define function 
function getAllCarYearInfo(inventory) {

    // It will check inventory not an array or not empty
    if (!Array.isArray(inventory) || inventory.length === 0) {
        console.log("Invalid inventory. Please provide a non-empty array.");
        return []; // Return an empty array if inventory is invalid or empty
    }
    
    let carYearArr = [];

    //copy car year from inventory 
    for (let carIndex = 0; carIndex < inventory.length; carIndex++) {
        carYearArr.push(inventory[carIndex].car_year);
    }

    //return array that contain all car years 
    return carYearArr;
}

//export function
module.exports = { getAllCarYearInfo };